/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.amirarcane.lockscreen.util;


import ohos.app.Context;
import ohos.global.resource.*;
import java.io.IOException;



public class ResUtil {

    private ResUtil() {
    }

    /**
     * get the path from id
     *
     * @param context the context
     * @param id      the id
     * @return the path from id
     */
    public static String getPathById(Context context, int id) {
        String path = "";
        if (context == null) {

            return path;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {
            return path;
        }
        try {
            path = manager.getMediaPath(id);
        } catch (IOException e) {

        } catch (NotExistException e) {

        } catch (WrongTypeException e) {

        }
        return path;
    }

    /**
     * get the color
     *
     * @param context the context
     * @param id      the id
     * @return the color
     */
    public static int getColor(Context context, int id) {
        int result = 0;
        if (context == null) {

            return result;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {

            return result;
        }
        try {
            result = manager.getElement(id).getColor();
        } catch (IOException e) {

        } catch (NotExistException e) {

        } catch (WrongTypeException e) {

        }
        return result;
    }

    /**
     * get the dimen value
     *
     * @param context the context
     * @param id      the id
     * @return get the float dimen value
     */
    public static float getDimen(Context context, int id) {
        float result = 0;
        if (context == null) {

            return result;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {

            return result;
        }
        try {
            result = manager.getElement(id).getFloat();
        } catch (IOException e) {

        } catch (NotExistException e) {

        } catch (WrongTypeException e) {

        }
        return result;
    }


}

