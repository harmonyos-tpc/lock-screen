/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.amirarcane.lockscreen.util;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

public class UiUtil extends ComponentContainer {

    private Context mContext;

    public UiUtil(Context context) {
        super(context);

        mContext = context;
    }

    private static final int LAYOUT_CHANGED = 1 << 0;

    /*
     * This method is used to copy layout attributes from the layout config
     * @param shape of the component,color from resource table and radius
     * @return changes based on the provided with & height of layout config
     */
    public final ShapeElement getShapeElement(int shape, int color, float radius) {

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(shape);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(ResUtil.getColor(mContext, color)));
        shapeElement.setCornerRadius(radius);
        return shapeElement;
    }

}
