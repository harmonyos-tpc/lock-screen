package com.amirarcane.lockscreen.andrognito.pinlockview;

import com.amirarcane.lockscreen.ResourceTable;
import com.amirarcane.lockscreen.util.ResUtil;
import com.amirarcane.lockscreen.util.UiUtil;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import static ohos.agp.utils.LayoutAlignment.CENTER;

public class PinLockAdapter extends BaseItemProvider implements Component.ClickedListener,Component.ComponentStateChangedListener {

    private static final int VIEW_TYPE_NUMBER = 0;
    private final Context context;
    private DirectionalLayout.LayoutConfig cvlayoutConfig;
    private ComponentContainer Group_Backbutton;
    private CustomizationOptionsBundle mCustomizationOptionsBundle;
    private OnNumberClickListener mOnNumberClickListener;
    private OnDeleteClickListener mOnDeleteClickListener;
    private DirectionalLayout directional_delete_button;
    private Button bt;
    private int mPinLength;
    private int BUTTON_ANIMATION_DURATION = 150;
    private Image backbutton;
    private int[] mKeyValues;
    private boolean isinsub = false;
    private final int componentClicked = 18432;
     private int counter =0;
    private Font mTypeface = null;

    @Override
    public void onComponentStateChanged(Component component, int i) {

        switch (i) {
            case ComponentState.COMPONENT_STATE_HOVERED:
            case ComponentState.COMPONENT_STATE_CHECKED:
            case ComponentState.COMPONENT_STATE_FOCUSED:
            case ComponentState.COMPONENT_STATE_PRESSED:
            case ComponentState.COMPONENT_STATE_SELECTED:
            case componentClicked:
                if(component instanceof Button) {
                    component.setBackground(new UiUtil(context).getShapeElement(ShapeElement.OVAL, ResourceTable.Color_dot_filled, 40.0f));
                } else {
                    component.setBackground(new UiUtil(context).getShapeElement(ShapeElement.RECTANGLE, ResourceTable.Color_dot_filled, 40.0f));
                }
                break;
            case ComponentState.COMPONENT_STATE_DISABLED:
                break;
            default:
                if(component instanceof Button) {
                    component.setBackground(new UiUtil(context).getShapeElement(ShapeElement.OVAL, ResourceTable.Color_circle, 40.0f));
                }else{
                    component.setBackground(new UiUtil(context).getShapeElement(ShapeElement.RECTANGLE, ResourceTable.Color_circle, 40.0f));
                }
                break;
        }

    }
    public PinLockAdapter(Context context) {
        this.context =context;
        this.mKeyValues = getAdjustKeyValues(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0});

    }

    @Override
    public int getCount() {
        //return 4 to display the four rows to be displayed in the keypad
        return 4 ;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {

        ComponentContainer root = (ComponentContainer) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_table_layout, null, false);


        TableLayout tableLayout = (TableLayout) root.findComponentById(ResourceTable.Id_table_layout);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.
                LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);

        tableLayout.setMarginLeft(215);

        tableLayout.verifyLayoutConfig(layoutConfig);

        int width = (int) ResUtil.getDimen(context, ResourceTable.Float_md_colorchooser_circlesize);
        int height = (int)ResUtil.getDimen(context, ResourceTable.Float_md_colorchooser_circlesize);
        ComponentContainer.LayoutConfig cvlayout = new ComponentContainer.LayoutConfig(width, height);
        cvlayout.setMargins(20, 20, 20, 20);

        cvlayoutConfig = new DirectionalLayout.LayoutConfig(cvlayout);

        // reset counter to 0 once the 12 items are drawn & displayed on keypad
         if(counter ==12)
         {counter=0;}

        for (int position = 0; position <3; position++) {

            ComponentContainer Group_button = (ComponentContainer) LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_layout_number_item, null, false);

            bt = (Button) Group_button.findComponentById(ResourceTable.Id_button);
            //setting the shape of the button
            bt.setBackground(new UiUtil(context).getShapeElement(ShapeElement.OVAL, ResourceTable.Color_circle, 40.0f));
            bt.setComponentStateChangedListener(this::onComponentStateChanged);
            //setting font if applied
            if(mTypeface != null) {
                bt.setFont(mTypeface);
            }

            Group_button.setLayoutConfig(cvlayoutConfig);
            counter++;
            if (i == 3 && position == 0) {
                //hiding the bottom-left-corner numberpad display.
                bt.setVisibility(Component.HIDE);

            }
            else if (i == 3 && position == 1) {
               //setting bottom-middle component as 0
                bt.setText("0");
                bt.setTag(0);
                bt.setTextAlignment(TextAlignment.CENTER);
            }
            else if (i == 3 && position == 2) {
                //setting bottom-right-corner display as delete button for digits
                 Group_Backbutton = (ComponentContainer) LayoutScatter.getInstance(context)
                        .parse(ResourceTable.Layout_layout_delete_item, null, false);

                 backbutton = (Image) Group_button.findComponentById(ResourceTable.Media_ic_backspace);

               directional_delete_button =(DirectionalLayout) Group_Backbutton.findComponentById(ResourceTable.Id_directional_delete);
               directional_delete_button.setBackground(new UiUtil(context).getShapeElement(ShapeElement.RECTANGLE, ResourceTable.Color_circle, 40.0f));
               directional_delete_button.setComponentStateChangedListener(this::onComponentStateChanged);
               Group_Backbutton.setLayoutConfig(cvlayoutConfig);
               directional_delete_button.setAlignment(CENTER); directional_delete_button.setTag(-1);

               directional_delete_button.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {

                            if (mOnDeleteClickListener != null) {
                                mOnDeleteClickListener.onDeleteClicked();

                            }
                        }
                    });
               directional_delete_button.setLongClickedListener(new Component.LongClickedListener() {
                        @Override
                        public void onLongClicked(Component component) {
                            if (mOnDeleteClickListener != null) {
                                mOnDeleteClickListener.onDeleteLongClicked();
                            }
                        }
                    });
            } else {
                bt.setText(String.valueOf(counter));
                bt.setTextAlignment(TextAlignment.CENTER);
                bt.setTag(counter);
            }

            if (i == 3 && position == 2) {
                tableLayout.addComponent(Group_Backbutton);
            }
            else
            {
                tableLayout.addComponent((Component) Group_button);
            }
            bt.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (mOnNumberClickListener != null) {
                        mOnNumberClickListener.onNumberClicked((Integer) component.getTag());
                    }


                }
            });


        }

     return root;
    }

    public void setTypeFace(Font typeFace) {
        mTypeface = typeFace;
    }

    @Override

    public int getItemComponentType(int position) {
        return VIEW_TYPE_NUMBER;
    }

    public void setPinLength(int pinLength) {
        this.mPinLength = pinLength;
    }


    private int[] getAdjustKeyValues(int[] keyValues) {
        int[] adjustedKeyValues = new int[keyValues.length + 1];
        for (int i = 0; i < keyValues.length; i++) {
            if (i < 9) {
                adjustedKeyValues[i] = keyValues[i];
            } else {
                adjustedKeyValues[i] = -1;
                adjustedKeyValues[i + 1] = keyValues[i];
            }
        }
        return adjustedKeyValues;
    }


    public void setOnItemClickListener(OnNumberClickListener onNumberClickListener) {
        this.mOnNumberClickListener = onNumberClickListener;
    }


    public void setOnDeleteClickListener(OnDeleteClickListener onDeleteClickListener) {
        this.mOnDeleteClickListener = onDeleteClickListener;
    }


    public void setCustomizationOptions(CustomizationOptionsBundle customizationOptionsBundle) {
        this.mCustomizationOptionsBundle = customizationOptionsBundle;
    }
    @Override
    public void onClick(Component component) {
}


    public interface OnNumberClickListener {
        void onNumberClicked(int keyValue);
    }

    public interface OnDeleteClickListener {
        void onDeleteClicked();

        void onDeleteLongClicked();
    }

}
