package com.amirarcane.lockscreen.andrognito.pinlockview;

import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class ResourceUtils {
    ResourceUtils() {
        throw new AssertionError();
    }

    public static float getDimensionInPx(Context context, float dip) {
        return (int) (dip * DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().densityPixels);
    }
}
