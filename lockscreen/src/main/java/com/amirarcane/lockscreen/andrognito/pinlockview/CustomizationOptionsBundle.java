package com.amirarcane.lockscreen.andrognito.pinlockview;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;

public class CustomizationOptionsBundle {
    private Component textColor;
    private int textSize;
    private int buttonSize;
    private Element buttonBackgroundDrawable;
    private Element deleteButtonDrawable;
    private int deleteButtonWidthSize, deleteButtonHeightSize;
    private boolean showDeleteButton;
    private int deleteButtonPressesColor;

    public CustomizationOptionsBundle() {
    }


    public void setTextColor(Component textColor) {
        this.textColor = textColor;
    }


    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }


    public void setButtonSize(int buttonSize) {
        this.buttonSize = buttonSize;
    }


    public void setButtonBackgroundDrawable(Element buttonBackgroundDrawable) {
        this.buttonBackgroundDrawable = buttonBackgroundDrawable;
    }

    public void setDeleteButtonDrawable(Element deleteButtonDrawable) {
        this.deleteButtonDrawable = deleteButtonDrawable;
    }

    public void setDeleteButtonWidthSize(int deleteButtonWidthSize) {
        this.deleteButtonWidthSize = deleteButtonWidthSize;
    }

    public void setDeleteButtonHeightSize(int deleteButtonHeightSize) {
        this.deleteButtonHeightSize = deleteButtonHeightSize;
    }

    public void setShowDeleteButton(boolean showDeleteButton) {
        this.showDeleteButton = showDeleteButton;
    }

    public void setDeleteButtonPressesColor(int deleteButtonPressesColor) {
        this.deleteButtonPressesColor = deleteButtonPressesColor;
    }
}
