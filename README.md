## Lock Screen

## Introduction
Simple and beautiful Lock Screen library to set an check pin code.

Easily secure your app with Lock Screen library as easy as starting an intent.

Set Pin:

<img src="./images/set1.png" width=200 ></img>

Check Pin:

<img src="./images/enter1.png" width=200 ></img>

Lock Screen gets a 4 digit pin code from user at first running time. After that every time that you start the intent, It asks
for pin code.

## Usage Instruction

 Add EnterPinAbility.class to MainAbility 
  ```
 addActionRoute("action.transit", EnterPinAbility.class.getName());
 ```
 
 And intent can be Started as
 ```
 Intent intent = new Intent();
 intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("com.amirarcane.sample")
                        .withAbilityName("com.amirarcane.sample.MainAbility")
                        .withAction("action.transit")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
```
First it checks Pin was entered before or not if the pin was set then it will as for entering pin else it asks for setting pin.
If need to set pin again Start the intent like below.
```
     Intent intent = new EnterPinAbility().getIntent(MainAbilitySlice.this,SET_PIN);
                intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("com.amirarcane.sample")
                        .withAbilityName("com.amirarcane.sample.MainAbility")
                        .withAction("action.transit")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
```
SET_PIN is boolean make it true.

### Customization

If font is needed to set for the library try below:

First: put the font in resources Example:put in path
"\entry\src\main\resources\rawfile"

Second:Pass the name of the Font in FONT_TEXT and FONT_NUMBERS.
Example:FONT_TEXT = "ALEAWB.TTF",FONT_NUMBER = "BLKCHCRY.TTF"

```
Intent intent = new EnterPinAbility().getIntent(MainAbilitySlice.this,FONT_TEXT,FONT_NUMBER);
                intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("com.amirarcane.sample")
                        .withAbilityName("com.amirarcane.sample.MainAbility")
                        .withAction("action.transit")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
```
If setting pin and setting font both are required then try below:
```
Intent intent = new EnterPinAbility().getIntent(MainAbilitySlice.this,SET_PIN,FONT_TEXT,FONT_NUMBER);
              intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
              Operation operation = new Intent.OperationBuilder()
                      .withBundleName("com.amirarcane.sample")
                      .withAbilityName("com.amirarcane.sample.MainAbility")
                      .withAction("action.transit")
                      .build();
              intent.setOperation(operation);
              startAbility(intent);
```
## Installation instruction
Method 1:
Generate the .har package through the library and add the .har package to the libs folder.
Add the following code to the entry gradle:
```
    implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
```
Method 2:
In project level build.gradle:
```
    allprojects{
        repositories{
            mavenCentral()
        }
    }
```
Add the following code to the entry gradle:
```
    implementation 'io.openharmony.tpc.thirdlib:lock-screen:1.0.2'
```