/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.amirarcane.sample.slice;

import com.amirarcane.lockscreen.ability.EnterPinAbility;
import com.amirarcane.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;

public class MainAbilitySlice extends AbilitySlice {

private Boolean SET_PIN = true;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false);
        super.setUIContent(rootLayout);
        String FONT_TEXT = "ALEAWB.TTF";
        String FONT_NUMBER = "BLKCHCRY.TTF";



        Button SetPin = (Button) rootLayout.findComponentById(ResourceTable.Id_setPin);
        Button normal = (Button) rootLayout.findComponentById(ResourceTable.Id_normal);
        Button setPinAndFont = (Button) rootLayout.findComponentById(ResourceTable.Id_setPinAndFont);
        Button setFont = (Button)  rootLayout.findComponentById(ResourceTable.Id_setFont);


        SetPin.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                //For setting pin pass SET_PIN true
                Intent intent =  EnterPinAbility.getIntent(MainAbilitySlice.this,SET_PIN);
                intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("com.amirarcane.sample")
                        .withAbilityName("com.amirarcane.sample.MainAbility")
                        .withAction("action.transit")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);

            }
        });
        normal.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("com.amirarcane.sample")
                        .withAbilityName("com.amirarcane.sample.MainAbility")
                        .withAction("action.transit")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);

            }
        });
      setPinAndFont.setClickedListener(new Component.ClickedListener() {
          @Override
          public void onClick(Component component) {
              //For setting pin pass SET_PIN true
              Intent intent = EnterPinAbility.getIntent(MainAbilitySlice.this,SET_PIN,FONT_TEXT,FONT_NUMBER);
              intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
              Operation operation = new Intent.OperationBuilder()
                      .withBundleName("com.amirarcane.sample")
                      .withAbilityName("com.amirarcane.sample.MainAbility")
                      .withAction("action.transit")
                      .build();
              intent.setOperation(operation);
              startAbility(intent);

          }
      });
        setFont.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Intent intent = EnterPinAbility.getIntent(MainAbilitySlice.this,FONT_TEXT,FONT_NUMBER);
                intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("com.amirarcane.sample")
                        .withAbilityName("com.amirarcane.sample.MainAbility")
                        .withAction("action.transit")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);

            }
        });


    }

    @Override
    public void onActive() {

        super.onActive();


    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

}
